// Considerando que no estaba seguro si querian la información de los libros, o de la serie
// Cree este algoritmo que toma la informacion general (ambos), la uniera y la guardara en 
// la base de datos.

import request from 'request';

import House from '../models/house';
import Book from '../models/book';
import Character from '../models/character';

export default async () => {
  console.log('\nCleaning Database...');

  await Book.remove({}, () => {
    console.log('DB.Book cleaned');
    return;
  });
  await House.remove({}, () => {
    console.log('DB.House cleaned');
    return;
  });
  await Character.remove({}, () => {
    console.log('DB.Character cleaned');
    return;
  });

  console.log('\nPoblating Database...');

  // Get the GOT Data
  const data = await new Promise((resolve) => {
    request({
      method: 'GET',
      uri: "https://api.got.show/api/general/characters",
    }, (error, response) => {
      resolve(JSON.parse(response.body));
    });
  });

  // slit in descriptive variable names the info
  const { book: bookCharacters, show: showCharacters } = data;

  // creation of a Map to store the characters information
  const characterMap = new Map();
  // creation of multiple sets to avoid duplication errors
  const nameSet = new Set();
  const bookSet = new Set();
  const houseSet = new Set();

  // iterate over the book's characters and getting relevant data of it
  bookCharacters.map(({ name, slug, gender, titles, books, pagerank, house, image }) => {

    // store the book name in the bookSet
    if (books.length > 0) {
      books.map((book) => {
        if (book) {
          bookSet.add(book);
        }
      });
    }

    // Clean and save the house name
    let houseName = 'undefined';
    if (house) {
      houseName = house.replace('&apos;', "'");
    }
    houseSet.add(houseName);

    // Save the name
    nameSet.add(name);

    // Save the relevant data on the Map
    characterMap.set(name, { slug, gender, titles, books, house: houseName, bookRank: pagerank.rank, image, showRank: 0 });
  });

  // iterate over the show's characters and getting relevant data of it
  showCharacters.map(({ name, slug, gender, titles, pagerank, house, image }) => {

    // Clean and save the house name
    let houseName = 'undefined';
    if (house) {
      houseName = house.replace('&apos;', "'");
    }
    houseSet.add(houseName);

    // if was'nt in the Set, save the relevant data
    if (!nameSet.has(name)) {
      nameSet.add(name);
      characterMap.set(name, { slug, gender, titles, house: houseName, showRank: pagerank.rank, image });
    }
    // if is in the Set, save the remaining data
    else {
      const characterData = characterMap.get(name);
      const { rank } = pagerank;
      characterData.showRank = rank;
      if (!characterData.image) {
        characterData.image = image;
      }
      characterMap.set(name, characterData);
    }
  });

  // create an array of promises wich will store the books in the DB
  // return the name and id of each one
  const booksToSave = [...bookSet].map((bookName) => new Promise((resolve) => {
    const book = new Book({ name: bookName });
    book.save((err, bookDB) => {
      if (err) {
        resolve(false);
      }
      if (!bookDB) {
        resolve(false);
      }
      else {
        resolve({ name: bookDB.name, id: bookDB._id });
      }
    });
  }));

  // wait for all promises and gets the info saved in the DB.
  const booksDB = await Promise.all(booksToSave);

  // create an array of promises wich will store the houses in the DB
  // return the name and id of each one
  const housesToSave = [...houseSet].map((houseName) => new Promise((resolve) => {
    // console.log(houseName);
    const house = new House({ name: houseName });
    house.save((err, houseDB) => {
      if (err) {
        resolve(false);
      }
      if (!houseDB) {
        resolve(false);
      }
      else {
        resolve({ name: houseDB.name, id: houseDB._id });
      }
    });
  }));

  // wait for all promises and gets the info saved in the DB.
  const housesDB = await Promise.all(housesToSave);

  // traditional function
  function searchHouse(array, name) {
    return array.find((house) => house.name === name);
  }

  // arrow function
  const searchBook = (array, name) => array.find((book) => book.name === name);

  // will store the information of the characters
  const arrayOfCharacters = [];

  // iterate over the Map with the character's information
  // to get the respective foreigns key 
  // of houses and books
  characterMap.forEach((value, key) => {
    const books = [];
    if (value.books) {
      const booksId = value.books.map((book) => {
        if (book) {
          return searchBook(booksDB, book).id;
        }
      });
      booksId.map((bookId) => {
        if (bookId) {
          books.push(bookId);
        }
      });
    }

    let houseName = 'undefined';

    if (value.house) {
      houseName = value.house;
    }

    const houseId = searchHouse(housesDB, houseName).id;

    // use of spread operator to destructure the value object
    // to create a new object, adding and replacing information
    arrayOfCharacters.push({ ...value, name: key, books, house: houseId });
  });

  // Due duplication errors, i must to create this algorithm to search and join duplicated.
  const setCheckerSlug = new Set();
  const arrayCleaned = [];
  const undefinedHouseId = housesDB.find((house) => house.name === 'undefined').id;

  // iterate over the characters's information
  arrayOfCharacters.map((characterData) => {
    // if the set with slugs names has'nt the slug
    // save it
    if (!setCheckerSlug.has(characterData.slug)) {
      setCheckerSlug.add(characterData.slug);
      arrayCleaned.push(characterData);
    }
    else {
      // go to find the index, create a copy of the array and get the duplicated character
      const duplicatedIndex = arrayCleaned.findIndex((character) => character.slug == characterData.slug);
      const duplicated = arrayCleaned.slice(0)[duplicatedIndex];

      // if the duplicated had some missing information
      // will be filled with the new information
      if (!duplicated.bookRank) {
        duplicated.bookRank = characterData.bookRank;
      }
      if (!duplicated.titles) {
        duplicated.titles = characterData.titles;
      }
      if (!duplicated.showRank) {
        duplicated.showRank = characterData.showRank;
      }
      if (duplicated.house === undefinedHouseId) {
        duplicated.house = characterData.house;
      }
      if (!duplicated.image) {
        duplicated.image = characterData.image;
      }
      if (!duplicated.gender) {
        duplicated.gender = characterData.gender;
      }

      // Replace into the array the new filled character
      arrayCleaned[duplicatedIndex] = duplicated;
    }
  });

  // After all data handling, store the information in the database
  arrayCleaned.map((characterData) => {
    const character = new Character(characterData);

    character.save((err, characterDB) => {
      if (err) {
        console.log(err);
        // console.log('error' + characterDB);
      }
      if (!characterDB) {
        // console.log('error' + characterDB);
      }
      else {
        // console.log('+');
      }
    });
  });

  return true;
}
