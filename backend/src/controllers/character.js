import express from 'express';

import { toBoolean, isMongoId } from 'validator';

import Character from '../models/character';

const server = express();

server.get('/characters/:ID', async (req, res) => {

  const { ID } = req.params;
  // Check if the id is valid
  if (!isMongoId(ID)) {
    res.status(400).json({
      ok: false,
      message: 'The id provided is\'nt valid',
    });
  }

  // get character info by id, ignoring the __V attribute
  // populating the house and book field 
  Character.findById(ID, { __v: 0 }).populate('house').populate('books').exec((err, characterDB) => {
    if (err) {
      res.status(400).json({
        ok: false,
        err: { message: 'internal database error' },
      });
    }
    else if (!characterDB) {
      res.status(400).json({
        ok: false,
        err: { message: 'character not found' },
      });
    }
    else if (!toBoolean(characterDB.toString())) {
      res.status(404).json({
        ok: false,
        err: { message: 'document not found' },
      });
    }
    else {
      // if all was OK, send the character data
      res.json({
        ok: true,
        character: characterDB,
      });
    }
  });
});


server.get('/characters', async (req, res) => {
  // get all characters, requesting only the name and the house
  // populating it
  Character.find({}, { name: 1, house: 1 }).populate('house').exec((err, arraycharacterDB) => {
    if (err) {
      res.status(400).json({
        ok: false,
        err: { message: 'internal database error' },
      });
    }
    else if (!arraycharacterDB) {
      res.status(400).json({
        ok: false,
        err: { message: 'characters not found' },
      });
    }
    else if (!toBoolean(arraycharacterDB.toString())) {
      res.status(404).json({
        ok: false,
        err: { message: 'document not found' },
      });
    }
    else {
      // if all was OK, clear the character data
      const cleanedCharacters = arraycharacterDB.map((character) => (
        { id: character._id, name: character.name, house: character.house.name }
      ));

      // count the documents
      Character.countDocuments({}, (error, count) => {
        // send the information
        res.json({
          ok: true,
          characters: cleanedCharacters,
          count
        });
      });
    }
  });
});

module.exports = server;
