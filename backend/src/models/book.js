import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const bookSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: [true, 'The name is necesary'],
  },
},/* eslint-disable */
  { collection: 'book' }
  /* eslint-enable */
);

bookSchema.plugin(uniqueValidator);

module.exports = mongoose.model('book', bookSchema);