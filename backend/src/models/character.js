import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;
const { ObjectId } = Schema.Types;

const characterSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: [true, 'The name is necesary'],
  },
  slug: {
    type: String,
    unique: true,
    required: true,
  },
  image: {
    type: String,
  },
  bookRank: {
    type: Number,
    default: 0,
  },
  showRank: {
    type: Number,
    default: 0,
  },
  books: [
    {
      type: ObjectId,
      ref: 'book',
    },
  ],
  gender: {
    type: String,
    // required: true,
  },
  house: {
    type: ObjectId,
    ref: 'house',
    required: true,
  },
  titles: [
    {
      type: String,
    },
  ],
},
  /* eslint-disable */
  { collection: 'character' }
  /* eslint-enable */
);

characterSchema.plugin(uniqueValidator);

module.exports = mongoose.model('character', characterSchema);
