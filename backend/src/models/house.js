import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const houseSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: [true, 'The name is necesary'],
  },
},/* eslint-disable */
  { collection: 'house' }
  /* eslint-enable */
);

houseSchema.plugin(uniqueValidator);

module.exports = mongoose.model('house', houseSchema);