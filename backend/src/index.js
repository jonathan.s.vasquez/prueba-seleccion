import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import mongoose from 'mongoose';

// Configuration imports
import * as config from './config'; // Server config

import poblateDB from './utils/poblateDB';

let server;

const startServer = async () => {

  server = express();

  const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
  }

  // enable cors
  server.use(cors(corsOptions));

  // monitoring request
  server.use(morgan(':method :url :status :response-time ms - :res[content-length]'));

  // database connection
  const databasesConnection = await mongoConnection().then((result) => {
    return result;
  });

  if (!databasesConnection) {
    // if cannot connect with the database, terminate the execution
    console.log('Database error');
    process.exit(1);
  }

  // API
  server.use(require('./controllers/index'));

  // Automatic poblate of database
  await poblateDB();

  server.listen(config.port, () => console.log(`\nServer is running in port ${config.port}... \n`));
}

// Connects to the mongoDB
var mongoConnection = () => {
  return new Promise((resolve, reject) => {
    const opciones = { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true };
    mongoose.connect(config.urlDB, opciones, (err) => {
      if (!err) {
        resolve();
      }
      else {
        reject();
      }
    });
  }).then(
    () => true,
    () => false,
  );
};

startServer();
