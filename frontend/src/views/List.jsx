import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class List extends React.Component {

  state = {
    loading: true,
    characters: [],
    page: 1,
    nameFilter: '',
    houseFilter: '',
  }

  async componentDidMount() {
    const response = await axios.get(
      'http://127.0.0.1:3004/characters',
    );
    const cambios = { loading: false };
    if (!response.data.ok) {
      console.log('there is a problem');
    }
    else {
      cambios.characters = response.data.characters;
    }

    this.setState(cambios);
  }

  handleGoToPrevious = () => {
    const { page } = this.state;
    if (page > 1) {
      this.setState({ page: page - 1 });
    }
  }

  handleGoToNext = () => {
    const { page, characters } = this.state;
    if (page < characters.length / 10) {
      this.setState({ page: page + 1 });
    }
  }

  handleChangeNameFilter = (data) => {
    this.setState({ nameFilter: data.value })
  }

  handleChangeHouseFilter = (data) => {
    this.setState({ houseFilter: data.value })
  }

  render() {
    const { loading, characters, page, nameFilter, houseFilter } = this.state;

    if (loading) {
      return (
        <h3>
          loading...
        </h3>
      )
    }

    // Filter on every update of the filters
    const nameFiltered = characters.filter((character) => character.name.toLowerCase().includes(nameFilter.toLowerCase()));
    const houseFiltered = nameFiltered.filter((character) => character.house.toLowerCase().includes(houseFilter.toLowerCase()));

    // selection of the characters of the page, based on state's page variable
    const pageCharacters = houseFiltered.slice(10 * (page - 1), 10 * page);

    return (
      <>
        <h1>List of characters</h1>
        <h3>Filter By</h3>
        <div style={{ display: 'flex' }}>
          <label htmlFor="name">Name:</label>
          <input id="name" name="name" type="text" label="Name" style={{ marginRight: 40 }} onChange={({ target }) => { this.handleChangeNameFilter(target) }} />
          <label htmlFor="house">House:</label>
          <input id="house" house="house" type="text" label="house" style={{ marginRight: 40 }} onChange={({ target }) => { this.handleChangeHouseFilter(target) }} />
        </div>
        <div>
          <div style={{ display: 'flex', marginTop: '20px' }}>
            <button type="button" onClick={this.handleGoToPrevious}>
              Previous page
          </button>
            <table style={{ width: 500, margin: '0px 20px 0px 20px', border: '1px dashed black', borderCollapse: 'collapse', textAlign: 'center', verticalAlign: 'center' }}>
              <thead>
                <th>Name</th>
                <th>House</th>
              </thead>
              <tbody>
                {/* iterate over the characters of the selected page */}
                {/* and show the info as table rows */}
                {pageCharacters.map(({ name, house, id }) => {
                  return (<tr>
                    <td><Link to={({ pathname: '/view', state: { characterId: id } })}>{name}</Link></td>
                    <td>{house}</td>
                  </tr>)
                })}
              </tbody>
            </table>
            <button type="button" onClick={this.handleGoToNext}>
              Next page
          </button>
          </div>
          <div style={{ marginLeft: '350px' }}>page {page}</div>
        </div>
      </>
    )
  }
}

export default List;


