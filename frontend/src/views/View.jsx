import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

@withRouter
class View extends React.Component {

  state = {
    loading: true,
    data: {},
  }

  async componentDidMount() {
    // Get the character data from the location state
    // provided by react-router-dom
    const { characterId } = this.props.location.state;
    // Get the data using axios
    const response = await axios.get(
      `http://127.0.0.1:3004/characters/${characterId}`,
    );

    const cambios = { loading: false };

    if (!response.data.ok) {
      console.log('there is a problem');
    }
    else {
      cambios.data = response.data.character;
    }

    this.setState(cambios);
  }

  handleGoBack = () => {
    this.props.history.push('/');
  }

  render() {
    const { loading, data } = this.state;

    if (loading) {
      return (
        <h3 style={{ margin: '0 auto' }}>
          loading...
        </h3>
      )
    }

    return (
      <>
        <button type="button" onClick={this.handleGoBack}>
          Go back
        </button>
        <h1>Details of the character</h1>
        {/* Conditional rendering */}
        {data.image &&
          <img src={`${data.image}`} style={{ objectFit: 'contain', width: 250, height: 250 }} />
        }
        <ul>

          <li>Name: {data.name}</li>
          <li>Slug: {data.slug}</li>
          <li>Gender: {data.gender}</li>

          {/* Conditional rendering */}
          {data.titles.length > 0 &&
            <>
              <li>Titles:</li>
              <ul>
                {data.titles.map((title) => <li>{title}</li>)}
              </ul>
            </>
          }
          {/* Conditional rendering */}
          {data.books.length > 0 &&
            <>
              <li>Books:</li>
              <ul>
                {data.books.map((book) => <li>{book.name}</li>)}
              </ul>
            </>
          }

          <li>Book Rank: {data.bookRank}</li>
          <li>Show Rank: {data.showRank}</li>
          <li>House: {data.house.name}</li>
        </ul>
      </>
    )
  }
}

export default View;


