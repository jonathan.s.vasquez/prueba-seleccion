import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import List from './views/List';
import View from './views/View';

function Providers() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path="/view">
            <View />
          </Route>
          <Route path="/list">
            <List />
          </Route>
          <Route path="/">
            <List />
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default Providers;
