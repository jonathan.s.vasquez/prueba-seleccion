var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/src/html/index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry: ["@babel/polyfill", path.resolve(__dirname, './react.jsx')],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js'
  },
  plugins: [
    HTMLWebpackPluginConfig,
  ],
  devServer: {
    contentBase: './dist',
    host: '0.0.0.0', //your ip address
    port: 8080,
    hot: true,
    historyApiFallback: true,
    disableHostCheck: true,
  },
};